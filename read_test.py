import numpy as np

# test the commit
# add a new commit

arr = np.load('sample.npy')

print (arr.dtype)

print arr['length']

print arr

a = arr[ arr['width'] > 0 ]
print ' arr is: '
print arr
print '\n'

print ' a is: '
print a
print '\n'

b = arr[np.logical_or.reduce(
        [
        arr['width'] > 0,
        arr['length'] >10,
        ]
       )]

print ' arr is: '
print arr
print '\n'

print ' b is: '
print b
print '\n'

c = arr[np.logical_or((arr['width'] > 0),(arr['cutCategory'] == 1))]
print ' c is: '
print c
print '\n'
