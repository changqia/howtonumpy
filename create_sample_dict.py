from array import array
import numpy as np

# Output Arrays
# ---------------------------------------------------
Content_List = ['cutCategory', 'length', 'width']

# for i in Pre:
#    for j in BTag:
#       VBF_string = 'yybb_%s%s_vbf_selected'%(i,j)
#       GGF_sring  = 'yybb_nonRes_XGBoost_%s%s_Cat'%(i,j)
#       Content_List.append(VBF_string)
#       Content_List.append(GGF_string)
#       for k in BDT:
#         GGF_sring  = 'yybb_nonRes_XGBoost_%s%s_Cat'%(i,j)+'_4GGF_VBF'+k
#         GGF_sring  = 'yybb_nonRes_XGBoost_%s%s_Cat'%(i,j)+'_4GGF'+k



Content = {}
for c in Content_List:
  Content[c] = np.array([])
#np_cutCategory = np.array([])
#np_length = np.array([])
#np_width = np.array([])

val_cutCategory = [   1,    2,    2,   1,   3,    1,    4 ]
val_length =      [ 11., 11.5, 16.1, 9.2, 8.7,  4.3,  2.5 ]
val_width =       [  6.,  6.5, 11.1, 4.2, 3.7, -0.7, -2.5 ]

for i,val in enumerate(val_cutCategory):
  length = val_length[i]
  width  =  val_width[i]
  Content['cutCategory'] = np.append(Content['cutCategory'], val)
  Content['length']      = np.append(Content['length'] , length)
  Content['width']       = np.append(Content['width']  , width)

  # for i in Pre:
  #    for j in BTag:
  #       VBF_string = 'yybb_%s%s_vbf_selected'%(i,j)
  #       GGF_sring  = 'yybb_nonRes_XGBoost_%s%s_Cat'%(i,j)
  print 'Cat: %d length: %3.1f width: %3.1f'%(val, length ,width)

nparrays = [
]

for c in Content_List:
  nparrays.append(Content[c])

recarray = np.core.records.fromarrays(nparrays, names=Content_List)

print recarray['length']

np.save('sample_dict.npy', recarray)
