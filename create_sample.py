from array import array
import numpy as np

# Output Arrays
# ---------------------------------------------------
np_cutCategory = np.array([])
np_length = np.array([])
np_width = np.array([])

# Create Structured array
names = [
  'cutCategory',
  'length',
  'width',
]

val_cutCategory = [   1,    2,    2,   1,   3,    1,    4 ]
val_length =      [ 11., 11.5, 16.1, 9.2, 8.7,  4.3,  2.5 ]
val_width =       [  6.,  6.5, 11.1, 4.2, 3.7, -0.7, -2.5 ]

for i,val in enumerate(val_cutCategory):
  length = val_length[i]
  width  =  val_width[i]
  np_cutCategory = np.append(np_cutCategory, val)
  np_length      = np.append(np_length, length)
  np_width       = np.append(np_width, width)
  print 'Cat: %d length: %3.1f width: %3.1f'%(val, length ,width)

nparrays = [
  np_cutCategory,
  np_length,
  np_width
]

recarray = np.core.records.fromarrays(nparrays, names=names)

print recarray['length']


np.save('sample.npy', recarray)
