import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields, repack_fields

arr = np.load('sample.npy')

print 'arr dtype'
print (arr.dtype)
print 'arr size'
print (arr.size)

print arr['length']

catEqualToOne = arr['cutCategory'] == 1
lengthAboveZero = arr['length'] > 10

arr = append_fields( arr, data = [catEqualToOne,lengthAboveZero ], names = ['catEqualToOne','lengthAboveZero'] )
print arr

print arr.dtype.names

print arr[2]['length']
